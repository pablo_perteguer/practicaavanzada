package com.example.practicapablo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;

public class MainActivity extends Activity implements OnClickListener, LocationListener
{
	private static final String PUBLISH_PERMISSION = "publish_actions";
	private Button m_CaptureButton;
	private ImageView m_imageView;
	private Bitmap m_foto;
	private LocationManager m_location;
	private Location m_Current;
	private TextView m_adress;
	private Button m_LoginButton;
	private Button m_LogoutButton;
	private Button m_PublishButton;
	private String m_currentInfo;
	

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_main );

		
		Session.StatusCallback statusCallback = new StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				updateUi();
			}
		};
		
        Session session = Session.getActiveSession();
        if (session == null) {
            if (savedInstanceState != null) {
                session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
            }
            if (session == null) {
                session = new Session(this);
            }
            Session.setActiveSession(session);
            
        }
		
		
		
		m_CaptureButton=(Button)findViewById( R.id.butonCapture );
		m_CaptureButton.setOnClickListener( MainActivity.this );
		m_imageView=(ImageView)findViewById( R.id.imagen );
		m_imageView.setOnClickListener( this );
		m_location = (LocationManager)getSystemService( Context.LOCATION_SERVICE );
		m_adress=(TextView)findViewById( R.id.geolocalizacion );
		m_LoginButton = (Button) findViewById(R.id.loginButton);
		m_LogoutButton = (Button) findViewById(R.id.logoutButton);
		m_PublishButton = (Button) findViewById(R.id.publishButton);
		
		// Login button
		m_LoginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Session session = Session.getActiveSession();
				if ( !session.isOpened() ) {
					doLogin();
				}
			}
		});
		
		// Logout button
		m_LogoutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Session session = Session.getActiveSession();
				if (session.isOpened()) {
					doLogout();
				}
			}
		});
		
		// publish button
		m_PublishButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				doPublish();
			}
		});

	}


     @Override
     public boolean onCreateOptionsMenu( Menu menu )
     {
          // Inflate the menu; this adds items to the action bar if it is
          // present.
          getMenuInflater().inflate( R.menu.main, menu );
          return true;
     }

   
     @Override
     protected void onResume()
     {
          super.onResume();
          if( m_location.isProviderEnabled( LocationManager.GPS_PROVIDER ) )
          {
               m_Current = m_location.getLastKnownLocation( LocationManager.GPS_PROVIDER );
               new LocationTask().execute( m_Current);
               m_location.requestLocationUpdates( LocationManager.GPS_PROVIDER,15 , 15, this );
          }
     
     
     
     }

     @Override
     public void onClick( View v )
     {
          
          if( v.getId()==R.id.butonCapture )
          {
               Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
               startActivityForResult( intent, 100 );
          }
          else if(v.getId()==R.id.imagen) 
          {
               ByteArrayOutputStream stream = new ByteArrayOutputStream();
               m_foto.compress( Bitmap.CompressFormat.PNG, 100, stream );
               byte[] byteArray = stream.toByteArray();   
               
               Intent intent= new Intent(this, zoom.class);
               intent.putExtra( zoom.fotoparam, byteArray );
               startActivity( intent );
               
          }
     }

     @Override
     protected void onActivityResult( int requestCode, int resultCode, Intent data )
     {
          if( requestCode==100 )
          {
               if( resultCode==RESULT_OK )
               {
                    Bundle Params = data.getExtras();
                     m_foto = (Bitmap)Params.get( "data" ); 
                    m_imageView.setImageBitmap( m_foto ); 
                    
               }
          }
          else {
               
          }
          super.onActivityResult( requestCode, resultCode, data );
     }

     private class LocationTask extends AsyncTask< Location, Void, String>
     {
          
         
          @Override
          protected String doInBackground( Location... Params )
          {
               String    LocationString = null;

               Geocoder geocoder = new Geocoder( MainActivity.this, Locale.getDefault());

               Location CurrentLocation = Params[ 0 ];
               List< Address > Addresses = null;
               try
               {
                    Addresses = geocoder.getFromLocation( CurrentLocation.getLatitude(), CurrentLocation.getLongitude(), 1 );
               }
               catch( IOException e )
               {

            	   
               }

               if( Addresses != null && Addresses.size() > 0 )
               {
            	   Address address = Addresses.get( 0 );
            	   LocationString = String.format("%s, %s, %s",
            			   address.getMaxAddressLineIndex() > 0 ? address.getAddressLine( 0 ) : "",
            					   address.getLocality(),
            					   address.getCountryName() );
               }
               return LocationString;
          }


          @Override
          protected void onPostExecute( String LocationInfo )
          {
        	  m_currentInfo =null ;
        	  if( LocationInfo != null )
        	  {
        		  m_currentInfo = "Latitude: " + m_Current.getLatitude() + "\r\nLongitude: " + m_Current.getLongitude() + "\r\n" + LocationInfo;
        		  m_adress.setText( m_currentInfo );
        	  }
        	  else m_adress.setText( "No hay se�al gps" );
          }
     }

 
     @Override
     public void onLocationChanged( Location location )
     {
          
          m_Current = location;
          if( m_Current != null ) new LocationTask().execute( m_Current );
          
     }

   
     @Override
     public void onProviderDisabled( String arg0 )
     {
     }


     @Override
     public void onProviderEnabled( String provider )
     {
     }


     @Override
     public void onStatusChanged( String provider, int status, Bundle extras )
     {
     }

     private void doLogin() {
 		Session session = Session.getActiveSession();
// 		SessionState state = session.getState();
 		if ( !session.isOpened() ) {
 			Session.OpenRequest request = new Session.OpenRequest(this);
 			request.setCallback(new StatusCallback() {
 				@Override
 				public void call(Session session, SessionState state, Exception exception) {
 					if (SessionState.OPENED.equals(state)) {
 						updateUi();
 					}
 					else if (exception != null) {
 						Log.e(MainActivity.class.getSimpleName(), "Error al hacer Facebook login: " + exception.getMessage());
 					}
 				}
 			});
 			
 			session.openForRead(request);
 		}
 		else {
 			updateUi();
 		}
 	}
 	
 	private void doLogout() {
 		Session.getActiveSession().closeAndClearTokenInformation();

 		updateUi();
 	}
 	
 	private void doPublish() {
 		Session session = Session.getActiveSession();
 		
 		if ( !session.getPermissions().contains( PUBLISH_PERMISSION ) ) {
 			
 			Session.NewPermissionsRequest request = new Session.NewPermissionsRequest(this, PUBLISH_PERMISSION);
 			request.setCallback(new StatusCallback() {
 				@Override
 				public void call(Session session, SessionState state, Exception exception) {
 					if (session.isOpened()) {
 						doPublish();
 					}
 					else if (exception != null) {
 						Log.e(MainActivity.class.getSimpleName(), "Error al hacer Facebook login: " + exception.getMessage());
 					}
 				}
 			});
 			
 			session.requestNewPublishPermissions(request);
 		}
 		
 		else {
 			Request request = Request.newStatusUpdateRequest(Session.getActiveSession(), "Mensaje de prueba!!! " + new Date().getTime(), new Request.Callback() {
 				@Override
 				public void onCompleted(Response response) {
 					if (response.getError() == null) {
 						Toast.makeText(MainActivity.this, response.getGraphObject().toString(), Toast.LENGTH_LONG).show();
 					}
 					else {
 						Toast.makeText(MainActivity.this, response.getError().getErrorMessage(), Toast.LENGTH_LONG).show();
 					}
 				}
 			});
 			
 			request.executeAsync();
 		}
 	}
 	
     private void updateUi() {
         Session session = Session.getActiveSession();
         if (session.isOpened()) {
 			m_LoginButton.setEnabled(false);
 			m_LogoutButton.setEnabled(true);
 			m_PublishButton.setEnabled(true);
 			
 			m_adress.setText(m_currentInfo);
 			m_imageView.setImageBitmap(m_foto);
 			
         }
         else {
     		m_LoginButton.setEnabled(true);
     		m_LogoutButton.setEnabled(false);
     		m_PublishButton.setEnabled(false);
     		
         }
     }
}
