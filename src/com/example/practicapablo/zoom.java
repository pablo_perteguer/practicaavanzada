
package com.example.practicapablo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;


public class zoom extends Activity

{    private ImageView m_image;
     private Bitmap m_Bitmap;
     public static String fotoparam= "fotoparam";
 
     @Override
     protected void onCreate( Bundle savedInstanceState )
     {
          super.onCreate( savedInstanceState );
          setContentView( R.layout.zoom );
          
          Bundle Params = getIntent().getExtras();
          byte[] byteArray = Params.getByteArray( fotoparam);

          m_Bitmap = BitmapFactory.decodeByteArray( byteArray, 0, byteArray.length );
          m_image = (ImageView)findViewById( R.id.imagen );
          m_image.setImageBitmap( m_Bitmap );
         
          
     }
    
}
